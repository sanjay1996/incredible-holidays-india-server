import ip from "ip";
import homePage from "../models/homePage";
import Categories from "../models/categories";
import ItinerarySchema from "../models/Itinerary";

function addHomePagedata(req, res) {
  let returnObj = {
    success: false,
    message: "Failed To update",
    data: {}
  };
  let bestValueTrips = [];
  let popularTrips = [];
  homePage
    .findOne({ pageName: "home" })
    .then(foundObj => {
      if (!foundObj) {
        const Homepage = new homePage({
          popularDestinations: [],
          bestValueTrips: bestValueTrips,
          popularTrips: popularTrips,
          counts: req.body.counts ? req.body.counts : {}
        });
        Homepage.save()
          .then(
            homePage
              .findOne({ pageName: "home" })
              .populate("bestValueTrips popularTrips")
              .then(foundObj => {
                returnObj.success = true;
                returnObj.message = "Updated Successfully";
                returnObj.data = foundObj;
                res.send(returnObj);
              })
          )
          .catch(err => {
            returnObj.success = true;
            returnObj.message = err;
            res.send(returnObj);
          });
      } else {
        if (req.body.bestValueTrips !== undefined) {
          foundObj.bestValueTrips = [...req.body.bestValueTrips];
        }
        if (req.body.popularTrips !== undefined) {
          foundObj.popularTrips = [...req.body.popularTrips];
        }
        if (req.body.counts !== undefined) {
          foundObj.counts = req.body.counts;
        }
        foundObj
          .save()
          .then(
            updatedObj => {
              if (updatedObj) {
                homePage
                  .findOne({ pageName: "home" })
                  .populate("bestValueTrips popularTrips")
                  .then(newObject => {
                    returnObj.success = true;
                    returnObj.message = "Updated Successfully";
                    returnObj.data = newObject;
                    res.send(returnObj);
                  });
              }
            }

            //   savedObj => {
            //   returnObj.success = true;
            //   returnObj.data = savedObj;
            //   returnObj.message = "Updated Successfully";
            //   res.send(returnObj);
            // }
          )
          .catch(err => {
            returnObj.success = true;
            returnObj.message = err;
            res.send(returnObj);
          });
      }
    })
    .catch(err => {
      returnObj.success = true;
      returnObj.message = err;
      res.send(returnObj);
    });
}

function popularDestinations(req, res) {
  let newDestinations = [];
  let Obj = {};

  var IP = ip.address();

  let returnObj = {
    success: false,
    data: [],
    message: "Failed To update"
  };
  req.files.map((item, index) => {
    Obj.state = req.body.state[index];
    Obj.imagePreviewUrl = `/uploads/${item.filename}`;
    newDestinations.push(Obj);
    Obj = {};
  });
  homePage
    .findOne({ pageName: "home" })
    .then(foundObj => {
      if (!foundObj) {
        const Homepage = new homePage({
          popularDestinations: newDestinations
        });
        Homepage.save()
          .then(newDestinations => {
            returnObj.success = true;
            returnObj.message = "Updated Successfully";
            returnObj.data = newDestinations.popularDestinations;
            res.send(returnObj);
          })
          .catch(err => {
            returnObj.success = true;
            returnObj.message = err;
            res.send(returnObj);
          });
      } else {
        foundObj.popularDestinations = [
          ...newDestinations,
          ...foundObj.popularDestinations
        ];
        foundObj
          .save()
          .then(newDestinations => {
            returnObj.success = true;
            returnObj.message = "Updated Successfully";
            returnObj.data = newDestinations.popularDestinations;
            res.send(returnObj);
          })
          .catch(err => {
            returnObj.success = true;
            returnObj.message = err;
            res.send(returnObj);
          });
      }
    })
    .catch(err => {
      returnObj.success = true;
      returnObj.message = err;
      res.send(returnObj);
    });
}

function getHomePagedata(req, res) {
  let returnObj = {
    success: false,
    message: "",
    data: {}
  };
  homePage
    .findOne({ pageName: "home" })
    .populate("bestValueTrips popularTrips")
    .then(foundObj => {
      returnObj.success = true;
      returnObj.message = "Success";
      returnObj.data = foundObj;
      res.send(returnObj);
    })
    .catch(err => {
      returnObj.success = false;
      returnObj.message = "Something went Wrong";
    });
}

function deletePopularDestination(req, res) {
  let returnObj = {
    success: false,
    message: "",
    data: {}
  };
  homePage.findOne({ pageName: "home" }).then(foundObj => {
    const index = req.body.index;
    let newArray = [
      ...foundObj.popularDestinations.slice(0, index),
      ...foundObj.popularDestinations.slice(index + 1)
    ];
    foundObj.popularDestinations = newArray;
    foundObj
      .save()
      .then(updated => {
        returnObj.success = true;
        returnObj.message = "Updated Successfully";
        returnObj.data = updated.popularDestinations;
        res.send(returnObj);
      })
      .catch(err => {
        returnObj.success = true;
        returnObj.message = err;
        res.send(returnObj);
      });
  });
}

function addCategory(req, res) {
  let returnObj = {
    success: false,
    message: "Failed To update"
  };
  let category = new Categories({
    categoryName: req.body.categoryName
  });
  category
    .save()
    .then(newCategory => {
      returnObj.success = true;
      returnObj.message = "Category Saved Successfully";
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.message = "Unable to Save";
      res.send(returnObj);
    });
}

function getCategoryList(req, res) {
  let returnObj = {
    success: false,
    message: "Error Fetching Categories",
    data: []
  };
  Categories.find()
    .then(foundItems => {
      returnObj.success = true;
      returnObj.message = `${foundItems.length} categories found`;
      returnObj.data = foundItems;
      res.send(returnObj);
    })
    .catch(error => {
      res.send(returnObj);
    });
}

function deleteCategory(req, res) {
  let returnObj = {
    success: false,
    message: "Unable To Delete"
  };
  Categories.findOneAndRemove({ _id: req.body._id })
    .then(() => {
      returnObj.success = true;
      returnObj.message = "Deleted Successfully";
      res.send(returnObj);
    })
    .catch(err => {
      returnObj.message = "Failed To Delete";
      res.send(returnObj);
    });
}

function getFooterData(req, res) {
  let returnObj = {
    success: false,
    message: "Error Fetching Data",
    data: []
  };
  let arr = [];
  Categories.find()
    .then(foundCategories => {
      const wait = foundCategories.length;
      foundCategories.map((categoryItem, index) => {
        ItinerarySchema.find({ category: categoryItem._id })
          .skip(0)
          .limit(3)
          .select("pageTitle slug")
          .then(foundItem => {
            let item = {
              _id: categoryItem._id,
              categoryName: categoryItem.categoryName,
              list: foundItem
            };
            arr = [...arr, item];
            if (wait - 1 == index) {
              returnObj.success = true;
              returnObj.message = `${foundCategories.length} Items Found`;
              returnObj.data = arr;
              res.send(returnObj);
            }
          });
      });
    })
    .catch(() => {
      res.send(returnObj);
    });
}

export default {
  addHomePagedata,
  popularDestinations,
  getHomePagedata,
  deletePopularDestination,
  addCategory,
  getCategoryList,
  deleteCategory,
  getFooterData
};
