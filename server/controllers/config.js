import configSchema from "../models/config";

function config(req, res) {
  let returnObj = {
    success: false,
    message: "data configuration failed",
    data: {}
  };
  configSchema.find().then(data => {
    if (data.length != 0) {
      if (req.body.key == "googleApi") {
        configSchema
          .findOneAndUpdate(
            { adminConfig: "admin" },
            {
              $set: {
                gaId: req.body.gaId,
                googleMapApi: req.body.googleMapApi
              }
            },
            { new: true }
          )
          .then(configData => {
            returnObj.success = true;
            returnObj.message = "data configured successfully";
            returnObj.data = configData;
            res.send(returnObj);
          })
          .catch(error => {
            returnObj.success = true;
            returnObj.message = error;
            res.send(returnObj);
          });
      } else if (req.body.key == "contactSettings") {
        configSchema
          .findOneAndUpdate(
            { adminConfig: "admin" },
            {
              $set: {
                title: req.body.title,
                email: req.body.email,
                mobile: req.body.mobile,
                address: req.body.address,
                website: req.body.website
              }
            },
            { new: true }
          )
          .then(configData => {
            returnObj.success = true;
            returnObj.message = "data configured successfully";
            returnObj.data = configData;
            res.send(returnObj);
          })
          .catch(error => {
            returnObj.success = true;
            returnObj.message = error;
            res.send(returnObj);
          });
      } else if (req.body.key == "socialMedia") {
        configSchema
          .findOneAndUpdate(
            { adminConfig: "admin" },
            {
              $set: {
                facebookUrl: req.body.facebookUrl,
                twitterUrl: req.body.twitterUrl,
                youtubeUrl: req.body.youtubeUrl,
                instagramUrl: req.body.instagramUrl
              }
            },
            { new: true }
          )
          .then(configData => {
            returnObj.success = true;
            returnObj.message = "data configured successfully";
            returnObj.data = configData;
            res.send(returnObj);
          })
          .catch(error => {
            returnObj.success = true;
            returnObj.message = error;
            res.send(returnObj);
          });
      }
    } else {
      const newConfig = new configSchema({
        gaId: req.body.gaId,
        googleMapApi: req.body.googleMapApi,
        title: req.body.title,
        email: req.body.email,
        mobile: req.body.mobile,
        address: req.body.address,
        website: req.body.website,
        facebookUrl: req.body.facebookUrl,
        twitterUrl: req.body.twitterUrl,
        youtubeUrl: req.body.youtubeUrl,
        instagramUrl: req.body.instagramUrl
      });
      newConfig
        .save()
        .then(configData => {
          returnObj.success = true;
          returnObj.message = "data configured successfully";
          returnObj.data = configData;
          res.send(returnObj);
        })
        .catch(error => {
          console.log("==============error======================");
          console.log(error);
          console.log("==============error======================");
        });
    }
  });
}

function fetchConfig(req, res) {
  let returnObj = {
    success: false,
    message: "No configuration Found",
    data: []
  };
  let configData = [];
  configSchema
    .findOne({ adminConfig: "admin" })
    .then(data => {
      configData = data;
      returnObj.success = true;
      returnObj.message = "configuration data Found";
      returnObj.data = configData;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "No configuration Found";
      res.send(returnObj);
    });
}

// 5b65880fb0cbf1f0df5ed9c0

export default {
  config,
  fetchConfig
};
