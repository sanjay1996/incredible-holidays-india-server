import profileSchema from "../models/profile";

function profile(req, res) {
  let returnObj = {
    success: false,
    message: "No profile found",
    data: {}
  };
  profileSchema.find().then(dataFound => {
    if (dataFound.length != 0) {
      profileSchema
        .findOneAndUpdate(
          { _id: req.body._id },
          {
            name: req.body.name,
            dateOfBirth: req.body.dateOfBirth,
            qualification: req.body.qualification,
            profilePicture:
              "http://localhost:4000/uploads/" + req.file.filename,
            about: req.body.about
          }
        )
        .then(profileData => {
          if (profileData == null) {
            res.send(returnObj);
          } else {
            returnObj.success = true;
            returnObj.message = "journey data updated successfully";
            returnObj.data = profileData;
            res.send(returnObj);
          }
        });
    } else {
      const newProfile = new profileSchema({
        name: req.body.name,
        dateOfBirth: req.body.dateOfBirth,
        qualification: req.body.qualification,
        profilePicture: "http://localhost:4000/uploads/" + req.file.filename,
        about: req.body.about
      });
      newProfile
        .save()
        .then(profileData => {
          returnObj.success = true;
          returnObj.message = "New profile saved";
          returnObj.data = profileData;
          res.send(returnObj);
        })
        .catch(error => {
          console.log("==============error======================");
          console.log(error);
          console.log("==============error======================");
        });
    }
  });
}

function getProfile(req, res) {
  let returnObj = {
    success: false,
    message: "No Profile Found",
    data: null
  };
  let profile = [];
  profileSchema
    .find()
    .then(foundProfile => {
      profile = foundProfile;
      returnObj.success = true;
      returnObj.message = "Profile Found";
      returnObj.data = profile;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "No Profile Found";
      res.send(returnObj);
    });
}

export default {
  profile,
  getProfile
};
