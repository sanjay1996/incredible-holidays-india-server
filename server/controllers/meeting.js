import MeetingSchema from "../models/meeting";

function meeting(req, res) {
  let returnObj = {
    success: false,
    message: "No meeting found",
    data: {}
  };
  const newMeeting = new MeetingSchema({
    venueName: req.body.venueName,
    venueDateTime: req.body.venueDateTime
  });
  newMeeting
    .save()
    .then(meetingData => {
      returnObj.success = true;
      returnObj.message = "New meeting saved";
      returnObj.data = meetingData;
      res.send(returnObj);
    })
    .catch(error => {
      console.log("==============error======================");
      console.log(error);
      console.log("==============error======================");
    });
}

function fetchMeeting(req, res) {
  let returnObj = {
    success: false,
    message: "No meeting Found",
    data: []
  };
  let meetingData = [];
  MeetingSchema.find()
    .then(data => {
      meetingData = data;
      returnObj.success = true;
      returnObj.message = "meeting data Found";
      returnObj.data = meetingData;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "No meeting Found";
      res.send(returnObj);
    });
}
export default {
  meeting,
  fetchMeeting
};
