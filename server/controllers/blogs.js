// import fs from "fs";
import BlogSchema from "../models/blogs";

function addBlog(req, res) {
  let returnObj = {
    success: false,
    message: "Failed To Add"
  };

  let blogSections = [];

  let slug = `${req.body.blogTitle} ${Math.floor(1000 + Math.random() * 9000)}`;
  slug = slug.replace(/ /g, "-");
  const featuredImage = `/uploads/${req.files.featuredImage[0].filename}`;

  if (req.files.blogImageOne !== undefined) {
    const item = {
      image: `/uploads/${req.files.blogImageOne[0].filename}`,
      description: req.body.blogTextOne
    };
    blogSections = [...blogSections, item];
  }
  if (req.files.blogImageTwo !== undefined) {
    const item = {
      image: `/uploads/${req.files.blogImageTwo[0].filename}`,
      description: req.body.blogTextTwo
    };
    blogSections = [...blogSections, item];
  }
  if (req.files.blogImageThree !== undefined) {
    const item = {
      image: `/uploads/${req.files.blogImageThree[0].filename}`,
      description: req.body.blogTextThree
    };
    blogSections = [...blogSections, item];
  }

  let Blog = new BlogSchema({
    blogHeader: req.body.blogHeader,
    blogTitle: req.body.blogTitle,
    metaTitle: req.body.metaTitle,
    slug: slug,
    metaDescription: req.body.metaDescription,
    metaKeywords: req.body.metaKeywords,
    featuredImage: featuredImage,
    blogSections: blogSections
  });
  Blog.save()
    .then(() => {
      returnObj.success = true;
      returnObj.message = "Blog Saved Successfully";
      res.send(returnObj);
    })
    .catch(err => {
      res.send(returnObj);
    });
}

function getBlogsList(req, res) {
  let returnObj = {
    success: false,
    message: "No Blogs Found",
    data: []
  };

  BlogSchema.find()
    .sort({ date: -1 })
    .then(foundItems => {
      returnObj.success = true;
      returnObj.message = `${foundItems.length} Blogs Found`;
      returnObj.data = foundItems;
      res.send(returnObj);
    })
    .catch(err => {
      res.send(returnObj);
    });
}

function getBlogsListWeb(req, res) {
  let returnObj = {
    success: false,
    message: "No Blogs Found",
    data: []
  };
  let skip = parseInt(req.query.pageNo) * 4;
  const limit = 4;
  BlogSchema.find()
    .sort({ date: -1 })
    .skip(skip)
    .limit(limit)
    .then(foundItems => {
      returnObj.success = true;
      returnObj.message = `${foundItems.length} Blogs Found`;
      returnObj.data = foundItems;
      res.send(returnObj);
    })
    .catch(err => {
      res.send(returnObj);
    });
}

function getBlogDetails(req, res) {
  let returnObj = {
    success: false,
    message: "No Details found",
    data: {}
  };
  BlogSchema.findOne({
    $or: [{ _id: req.headers._id }, { slug: req.headers.slug }]
  })
    .populate("reviews")
    .then(foundObj => {
      returnObj.success = true;
      returnObj.message = "Blog Found";
      returnObj.data = foundObj;
      res.send(returnObj);
    })
    .catch(err => {
      returnObj.message = "Something Went wrong";
      res.send(returnObj);
    });
}

function deleteBlog(req, res) {
  let returnObj = {
    success: false,
    message: "No Blog found"
  };
  BlogSchema.findOneAndRemove({ _id: req.body._id })
    .then(foundBlog => {
      if (foundBlog) {
        returnObj.success = true;
        returnObj.message = "Itinerary deleted";
        returnObj.data = ItineraryData;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    })
    .catch(err => {
      returnObj.message = "Failed to Delete";
      res.send(returnObj);
    });
}

function deleteBlogImage(req, res) {
  let returnObj = {
    success: false,
    message: "Unable To Delete Image",
    data: {}
  };
  BlogSchema.findOne({ _id: req.body._id })
    .then(foundObj => {
      if (req.body.type == "featured") {
        foundObj.featuredImage = null;
        foundObj
          .save()
          .then(newObj => {
            returnObj.success = true;
            returnObj.message = "Featured Image Deleted";
            returnObj.data = newObj;
            res.send(returnObj);
          })
          .catch(err => {
            res.send(returnObj);
          });
      } else {
        const index = req.body.index;
        foundObj.blogSections[index].image = null;
        foundObj
          .save()
          .then(newObj => {
            returnObj.success = true;
            returnObj.message = "Image Deleted";
            returnObj.data = newObj;
            res.send(returnObj);
          })
          .catch(err => {
            res.send(returnObj);
          });
      }
    })
    .catch(err => {
      res.send(returnObj);
    });
}
function editBlog(req, res) {
  let returnObj = {
    success: false,
    message: "Unable To Update",
    data: {}
  };

  BlogSchema.findOne({ _id: req.body._id })
    .then(foundObj => {
      foundObj.blogHeader = req.body.blogHeader;
      foundObj.blogTitle = req.body.blogTitle;
      foundObj.metaTitle = req.body.metaTitle;
      foundObj.metaDescription = req.body.metaDescription;
      foundObj.metaKeywords = req.body.metaKeywords;
      if (req.body.blogTextOne !== undefined) {
        foundObj.blogSections[0].description = req.body.blogTextOne;
      }
      if (req.body.blogTextTwo !== undefined) {
        foundObj.blogSections[1].description = req.body.blogTextTwo;
      }
      if (req.body.blogTextThree !== undefined) {
        foundObj.blogSections[2].description = req.body.blogTextThree;
      }
      if (req.files.featuredImage !== undefined) {
        foundObj.featuredImage = `/uploads/${
          req.files.featuredImage[0].filename
        }`;
      }

      if (req.files.blogImageOne !== undefined) {
        foundObj.blogSections[0].image = `/uploads/${
          req.files.blogImageOne[0].filename
        }`;
      }
      if (req.files.blogImageTwo !== undefined) {
        foundObj.blogSections[1].image = `/uploads/${
          req.files.blogImageTwo[0].filename
        }`;
      }
      if (req.files.blogImageThree !== undefined) {
        foundObj.blogSections[2].image = `/uploads/${
          req.files.blogImageThree[0].filename
        }`;
      }
      foundObj
        .save()
        .then(savedObj => {
          returnObj.success = true;
          returnObj.message = "Successfully Updated";
          returnObj.data = savedObj;
          res.send(returnObj);
        })
        .catch(err => {
          res.send(returnObj);
        });
    })
    .catch(err => {
      res.send(returnObj);
    });
}
export default {
  addBlog,
  getBlogsList,
  getBlogDetails,
  deleteBlog,
  deleteBlogImage,
  editBlog,
  getBlogsListWeb
};
