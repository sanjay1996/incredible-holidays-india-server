import enquiresSchema from "../models/enquiries";

function addEnquiry(req, res) {
  let returnObj = {
    success: false,
    message: "Something Went Wrong"
  };
  const newEnquiry = new enquiresSchema({
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    query: req.body.query,
    itinerary: req.body.itinerary
  });
  newEnquiry
    .save()
    .then(enquiryData => {
      returnObj.success = true;
      returnObj.message = "Successfully Posted";
      res.send(returnObj);
    })
    .catch(error => {
      res.send(returnObj);
    });
}

function getEnquires(req, res) {
  let returnObj = {
    success: false,
    message: "No Enquery Found",
    data: []
  };

  let enquery = [];
  enquiresSchema
    .find()
    .populate("itinerary", "pageTitle slug")
    .then(foundEnquery => {
      enquery = foundEnquery;
      returnObj.success = true;
      returnObj.message = "Enquery Found";
      returnObj.data = enquery;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "No Enquery Found";
      res.send(returnObj);
    });
}

function editEnquiryStatus(req, res) {
  let returnObj = {
    success: false,
    message: "Update Failed",
    data: {}
  };
  enquiresSchema
    .findByIdAndUpdate(
      { _id: req.body._id },
      { $set: { status: req.body.status } },
      { new: true }
    )
    .then(updated => {
      returnObj.success = true;
      returnObj.message = "Successfully Updated";
      returnObj.data = updated;
      res.send(returnObj);
    })
    .catch(err => {
      res.send(returnObj);
    });
}

export default {
  addEnquiry,
  getEnquires,
  editEnquiryStatus
};
