import ComplaintsSchema from "../models/complaints";

function complaints(req, res) {
  let returnObj = {
    success: false,
    message: "No complaint found",
    data: {}
  };
  const newComplaint = new ComplaintsSchema({
    name: req.body.name,
    email: req.body.email,
    mobileNo: req.body.mobileNo,
    complaint: req.body.complaint
  });
  newComplaint
    .save()
    .then(complaints => {
      returnObj.success = true;
      returnObj.message = "New complaint saved";
      returnObj.data = complaints;
      res.send(returnObj);
    })
    .catch(error => {
      console.log("==============error======================");
      console.log(error);
      console.log("==============error======================");
    });
}

function getComplaints(req, res) {
  let returnObj = {
    success: false,
    message: "No complaint found",
    data: {}
  };
  ComplaintsSchema.find().then(foundData => {
    returnObj.success = true;
    returnObj.message = "New poll saved";
    returnObj.data = foundData;
    res.send(returnObj);
  });
}

export default {
  complaints,
  getComplaints
};
