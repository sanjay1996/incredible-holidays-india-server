import UserSchema from "../models/user";
import ReviewSchema from "../models/reviews";
import ItinerarySchema from "../models/Itinerary";
import BlogSchema from "../models/blogs";

import bcrypt from "bcrypt";
const saltRounds = 10;

function createUser(req, res) {
  let returnObj = {
    success: false,
    message: "User Already Exist`s",
    data: {}
  };

  UserSchema.findOne({ email: req.body.Email })
    .then(foundUser => {
      if (foundUser != null) {
        res.send(returnObj);
      } else {
        bcrypt.hash(req.body.password, saltRounds).then(hashPassword => {
          const newUser = new UserSchema({
            name: req.body.name,
            phone: req.body.mobileNo,
            email: req.body.email,
            password: hashPassword
          });
          newUser.save().then(savedUser => {
            returnObj.success = true;
            returnObj.message = "Users Registered Successfully!";
            returnObj.data = savedUser;
            res.send(returnObj);
          });
        });
      }
    })
    .catch(error => {});
}

function fetchUser(req, res) {
  let returnObj = {
    success: false,
    message: "No user found",
    data: []
  };
  UserSchema.find().then(userFound => {
    returnObj.success = true;
    returnObj.message = "Users found";
    returnObj.data = userFound;
    res.send(returnObj);
  });
}

function reviews(req, res) {
  let returnObj = {
    success: false,
    message: "Review Not Submitted"
  };

  const newReview = new ReviewSchema({
    itineraryId: req.body.itineraryId,
    name: req.body.name,
    email: req.body.email,
    subject: req.body.subject,
    rating: req.body.rating,
    message: req.body.message,
    title: req.body.title,
    url: req.body.url
  });
  newReview
    .save()
    .then(userReview => {
      ItinerarySchema.findOne({ _id: userReview.itineraryId })
        .then(foundItem => {
          let newArr = [userReview._id, ...foundItem.reviews];
          foundItem.reviews = newArr;
          foundItem
            .save()
            .then(() => {
              returnObj.success = true;
              returnObj.message = "Thanks for Submitting Review";
              res.send(returnObj);
            })
            .catch(err => {
              res.send(returnObj);
            });
        })
        .catch(err => {
          res.send(returnObj);
        });
    })
    .catch(error => {
      res.send(returnObj);
    });
}

function getAllReviews(req, res) {
  let returnObj = {
    success: false,
    message: "No Review Found",
    data: []
  };
  let reviews = [];
  ReviewSchema.find()
    .then(foundItems => {
      reviews = foundItems;
      returnObj.success = true;
      returnObj.message = "Review Found";
      returnObj.data = reviews;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "Reviews not Found";
      res.send(returnObj);
    });
}

function reviewStatusUpdate(req, res) {
  let returnObj = {
    success: false,
    message: "Status not updated",
    data: {}
  };
  ReviewSchema.findOneAndUpdate(
    { _id: req.body._id },
    { $set: { status: req.body.status } },
    { new: true }
  ).then(statusUpdate => {
    if (statusUpdate == null) {
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.data = statusUpdate;
      returnObj.message = "status updated successfully";
      res.send(returnObj);
    }
  });
}

function reviewDelete(req, res) {
  let returnObj = {
    success: false,
    message: "No review found",
    data: {}
  };
  ReviewSchema.findOneAndRemove({ _id: req.body._id }).then(reviewRemoved => {
    if (reviewRemoved == null) {
      res.send(returnObj);
    } else {
      ItinerarySchema.findOne({ _id: reviewRemoved.itineraryId })
        .then(foundItem => {
          if (foundItem !== null) {
            let foundIndex = null;
            foundItem.reviews.map((item, index) => {
              if (item == req.body._id) {
                foundIndex = index;
              }
            });
            if (foundIndex !== null) {
              const newReviewsArray = [
                ...foundItem.reviews.slice(0, foundIndex),
                ...foundItem.reviews.slice(foundIndex + 1)
              ];
              foundItem.reviews = newReviewsArray;
              foundItem.save().then(() => {
                returnObj.success = true;
                returnObj.message = "Review deleted successfully";
                res.send(returnObj);
              });
            } else {
              returnObj.success = true;
              returnObj.message = "Review deleted successfully";
              res.send(returnObj);
            }
          } else {
            returnObj.success = true;
            returnObj.message = "Review deleted successfully";
            res.send(returnObj);
          }
        })
        .catch(err => {
          res.send(returnObj);
        });
    }
  });
}

function reviewBlog(req, res) {
  let returnObj = {
    success: false,
    message: "Review Not Submitted"
  };

  const newReview = new ReviewSchema({
    name: req.body.name,
    email: req.body.email,
    message: req.body.message,
    title: req.body.title,
    url: req.body.url
  });

  newReview
    .save()
    .then(userReview => {
      BlogSchema.findOne({ _id: req.body.blogId })
        .then(foundItem => {
          let newArr = [userReview._id, ...foundItem.reviews];
          foundItem.reviews = newArr;
          foundItem
            .save()
            .then(SavedItem => {
              returnObj.success = true;
              returnObj.message = "Thanks for Submitting Review";
              res.send(returnObj);
            })
            .catch(err => {
              res.send(returnObj);
            });
        })
        .catch(err => {
          res.send(returnObj);
        });
    })
    .catch(err => {
      res.send(returnObj);
    });
}

// function Itinerary(req, res) {
//     let returnObj = {
//         success: false,
//         message: "No data added",
//         data: {}
//     }
//     const newItinerary = new ItinerarySchema({
//         pageTitle: req.body.pageTitle,
//         metaTitle: req.body.metaTitle,
//         pageUrl: req.body.pageUrl,
//         metaDescription: req.body.metaDescription,
//         metaKeywords: req.body.metaKeywords,
//         description: req.body.description,
//         imagePath: "http://localhost:3000/uploads/" + req.file.filename,
//         itineraryDays: req.body.itineraryDays,
//         tripDetail: req.body.tripDetail,
//     });
//     newItinerary.save().then((ItineraryData) => {
//         returnObj.success = true;
//         returnObj.message = "Data added"
//         returnObj.data = ItineraryData;
//         res.send(returnObj)
//     }).catch((error) => {
//     });
// }

export default {
  createUser,
  fetchUser,
  reviews,
  // Itinerary,
  reviewStatusUpdate,
  getAllReviews,
  reviewDelete,
  reviewBlog
};
