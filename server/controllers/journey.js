import JourneySchema from "../models/journey";

function journey(req, res) {
  let returnObj = {
    success: false,
    message: "No journey found",
    data: {}
  };
  JourneySchema.find().then(dataFound => {
    if (dataFound.length != 0) {
      JourneySchema.findOneAndUpdate(
        { _id: req.body._id },
        { $set: { journey: req.body.journey, image: req.body.image } },
        { new: true }
      ).then(journeyData => {
        if (journeyData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.message = "journey data updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      const newJourney = new JourneySchema({
        journey: req.body.journey,
        image: req.body.image
      });
      newJourney.save().then(journeyData => {
        returnObj.success = true;
        returnObj.message = "journey data added successfully";
        returnObj.data = journeyData;
        res.send(returnObj);
      });
    }
  });
}

function getJourney(req, res) {
  let returnObj = {
    success: false,
    message: "No journey found",
    data: {}
  };
  JourneySchema.find().then(journeyData => {
    returnObj.success = true;
    returnObj.message = "journey data found";
    returnObj.data = journeyData;
    res.send(returnObj);
  });
}

export default {
  journey,
  getJourney
};
