import ReviewsSchema from "../models/reviews";
import moment from "moment";
// import Mongoose from "mongoose";
// Mongoose.promise = global.Promise;

function dashboardReviewCount(req, res) {
  let returnObj = {
    success: false,
    message: "No Data found",
    data: []
  };
  let Data = null;
  let Jan = 0;
  let Feb = 0;
  let Mar = 0;
  let Apr = 0;
  let May = 0;
  let Jun = 0;
  let Jul = 0;
  let Aug = 0;
  let Sept = 0;
  let Oct = 0;
  let Nov = 0;
  let Dec = 0;
  let currentYear = moment(Date()).format("YY");
  ReviewsSchema.find()
    .then(data => {
      data.forEach(allData => {
        if (currentYear == moment(allData.date).format("YY")) {
          if (moment(allData.date).format("MMM") == "Jan") {
            Jan = Jan + 1;
          } else if (moment(allData.date).format("MMM") == "Feb") {
            Feb = Feb + 1;
          } else if (moment(allData.date).format("MMM") == "Mar") {
            Mar = Mar + 1;
          } else if (moment(allData.date).format("MMM") == "Apr") {
            Apr = Apr + 1;
          } else if (moment(allData.date).format("MMM") == "May") {
            May = May + 1;
          } else if (moment(allData.date).format("MMM") == "Jun") {
            Jun = Jun + 1;
          } else if (moment(allData.date).format("MMM") == "Jul") {
            Jul = Jul + 1;
          } else if (moment(allData.date).format("MMM") == "Aug") {
            Aug = Aug + 1;
          } else if (moment(allData.date).format("MMM") == "Sep") {
            Sept = Sept + 1;
          } else if (moment(allData.date).format("MMM") == "Oct") {
            Oct = Oct + 1;
          } else if (moment(allData.date).format("MMM") == "Nov") {
            Nov = Nov + 1;
          } else if (moment(allData.date).format("MMM") == "Dec") {
            Dec = Dec + 1;
          }
        }
      });
      Data = {
        Jan: Jan,
        Feb: Feb,
        Mar: Mar,
        Apr: Apr,
        May: May,
        Jun: Jun,
        Jul: Jul,
        Aug: Aug,
        Sept: Sept,
        Oct: Oct,
        Nov: Nov,
        Dec: Dec
      };
    })
    .then(() => {
      returnObj.success = true;
      returnObj.message = "Monthly Reviews count";
      returnObj.data = Data;
      res.send(returnObj);
    });
}

export default {
  dashboardReviewCount
};
