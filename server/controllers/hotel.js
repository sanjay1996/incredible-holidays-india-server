import HotelSchema from "../models/hotel";

function addHotelDetails(req, res) {
  let returnObj = {
    success: false,
    message: "No data added",
    data: {}
  };
  let gallery = [];
  req.files.forEach(pics => {
    gallery.push("/uploads/" + pics.filename);
  });
  const newHotel = new HotelSchema({
    hotelName: req.body.hotelName,
    images: gallery,
    addressLine1: req.body.addressLine1,
    addressLine2: req.body.addressLine2,
    // email: req.body.email,
    // phone: req.body.phoneNo,
    city: req.body.city,
    state: req.body.state,
    country: req.body.country,
    zipcode: req.body.zipcode
  });
  newHotel
    .save()
    .then(hotelData => {
      returnObj.success = true;
      returnObj.message = "New Hotel added";
      returnObj.data = hotelData;
      res.send(returnObj);
    })
    .catch(error => {
      res.send(returnObj);
    });
}
function fetchHotelDetails(req, res) {
  let returnObj = {
    success: false,
    message: "No Hotel Found",
    data: []
  };
  let hotels = [];
  HotelSchema.find()
    .then(foundHotel => {
      hotels = foundHotel;
      returnObj.success = true;
      returnObj.message = "Hotel Found";
      returnObj.data = hotels;
      res.send(returnObj);
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = "No Hotel Found";
      res.send(returnObj);
    });
}

function deleteHotel(req, res) {
  let returnObj = {
    success: false,
    message: "No Hotel Found",
    data: []
  };
  HotelSchema.findOneAndRemove({ _id: req.body._id }).then(foundHotel => {
    if (foundHotel == null) {
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.message = "Hotel deleted";
      res.send(returnObj);
    }
  });
}

function updateHotel(req, res) {
  let returnObj = {
    success: false,
    message: "No Hotel Found",
    data: {}
  };
  let gallery = [];
  if (req.files.length > 0) {
    req.files.forEach(pics => {
      gallery.push("/uploads/" + pics.filename);
    });
  }

  HotelSchema.findOneAndUpdate(
    { _id: req.body._id },
    {
      $set: {
        hotelName: req.body.hotelName,
        addressLine1: req.body.addressLine1,
        addressLine2: req.body.addressLine2,
        // email: req.body.email,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country,
        zipcode: req.body.zipcode
      },
      $push: {
        images: {
          $each: gallery
        }
      }
    },
    { new: true, multi: true }
  ).then(hotelUpdate => {
    if (hotelUpdate == null) {
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.data = hotelUpdate;
      returnObj.message = "Hotel updated successfully";
      res.send(returnObj);
    }
  });
}
function deleteHotelImage(req, res) {
  let returnObj = {
    success: false,
    message: "Unable To Delete Image",
    data: []
  };
  HotelSchema.findOne({ _id: req.body._id })
    .then(foundObj => {
      const index = req.body.index;
      foundObj.images.splice(index, 1);
      foundObj.images = [...foundObj.images];
      foundObj
        .save()
        .then(savedObj => {
          returnObj.success = true;
          returnObj.message = "Image Deleted";
          returnObj.data = foundObj.images;
          res.send(returnObj);
        })
        .catch(err => {
          res.send(returnObj);
        });
    })
    .catch(err => {
      res.send(returnObj);
    });
}

function updateStatus(req, res) {
  let returnObj = {
    success: false,
    message: "status updation failed",
    data: []
  };
  HotelSchema.findOneAndUpdate(
    { _id: req.body._id },
    {
      $set: {
        status: req.body.status
      }
    },
    { new: true }
  ).then(statusUpdate => {
    if (statusUpdate == null) {
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.message = "status updated successfully";
      res.send(returnObj);
    }
  });
}
export default {
  addHotelDetails,
  fetchHotelDetails,
  deleteHotel,
  updateHotel,
  updateStatus,
  deleteHotelImage
};
