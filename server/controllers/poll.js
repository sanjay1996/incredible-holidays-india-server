import PollSchema from "../models/poll";

function poll(req, res) {
  let returnObj = {
    success: false,
    message: "No poll found",
    data: {}
  };
  const newPoll = new PollSchema({
    question: req.body.question,
    options: req.body.options
  });
  newPoll
    .save()
    .then(pollData => {
      returnObj.success = true;
      returnObj.message = "New poll saved";
      returnObj.data = pollData;
      res.send(returnObj);
    })
    .catch(error => {
      console.log("==============error======================");
      console.log(error);
      console.log("==============error======================");
    });
}

function getPoll(req, res) {
  let returnObj = {
    success: false,
    message: "No poll found",
    data: {}
  };
  PollSchema.find().then(foundPolls => {
    console.log("====================================");
    console.log(foundPolls);
    console.log("====================================");
  });
}

export default {
  poll,
  getPoll
};
