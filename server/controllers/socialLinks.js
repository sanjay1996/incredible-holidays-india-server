import SocialSchema from "../models/socialLinks";

function socialLink(req, res) {
  let returnObj = {
    success: false,
    message: "No social Link found",
    data: {}
  };
  SocialSchema.find().then(dataFound => {
    if (dataFound.length > 0) {
      SocialSchema.findOneAndUpdate(
        { _id: req.body._id },
        {
          $set: {
            facebookLink: req.body.facebookLink,
            twitterLink: req.body.twitterLink,
            youtubeLink: req.body.youtubeLink,
            instagramLink: req.body.instagramLink
          }
        },
        { new: true }
      ).then(socialData => {
        if (socialData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.message = "social details updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      const newSocialLink = new SocialSchema({
        facebookLink: req.body.facebookLink,
        twitterLink: req.body.twitterLink,
        youtubeLink: req.body.youtubeLink,
        instagramLink: req.body.instagramLink
      });
      newSocialLink.save().then(socialData => {
        returnObj.success = true;
        returnObj.message = "social details added successfully";
        returnObj.data = socialData;
        res.send(returnObj);
      });
    }
  });
}

function getsocialLink(req, res) {
  let returnObj = {
    success: false,
    message: "No data found",
    data: {}
  };
  SocialSchema.find().then(socialData => {
    returnObj.success = true;
    returnObj.message = "data found";
    returnObj.data = socialData;
    res.send(returnObj);
  });
}

export default {
  socialLink,
  getsocialLink
};
