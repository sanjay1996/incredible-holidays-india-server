import ItinerarySchema from "../models/Itinerary";
import HotelSchema from "../models/hotel";
import mongoose from "mongoose";

const ObjectID = mongoose.Types.ObjectId;
function Itinerary(req, res) {
  let returnObj = {
    success: false,
    message: "No data added",
    data: {}
  };
  let gallery = [];
  req.files.gallery.forEach(pics => {
    gallery.push("/uploads/" + pics.filename);
  });
  let featuredPic = null;
  req.files.featuredImage.forEach(pics => {
    featuredPic = "/uploads/" + pics.filename;
  });
  const itinerary = JSON.parse(req.body.itinerary);
  const tabs = JSON.parse(req.body.tabs);
  let slugvalue = `${req.body.pageTitle} ${Math.floor(
    1000 + Math.random() * 9000
  )}`;
  slugvalue = slugvalue.replace(/ /g, "-");
  let months = JSON.parse(req.body.month);
  let budgetCategory = JSON.parse(req.body.budgetCategory);

  let category = JSON.parse(req.body.category);
  const newItinerary = new ItinerarySchema({
    pageTitle: req.body.pageTitle,
    metaTitle: req.body.metaTitle,
    price: req.body.price,
    shortDescription: req.body.shortDescription,
    noOfPeople: req.body.noOfPeople,
    city: req.body.city,
    state: req.body.state,
    metaDescription: req.body.metaDescription,
    metaKeywords: req.body.metaKeywords,
    description: req.body.description,
    slug: slugvalue,
    featuredImage: featuredPic,
    gallery: gallery,
    itineraryDays: itinerary,
    tripDetail: tabs,
    category: category,
    month: months,
    budgetCategory: budgetCategory
  });
  newItinerary
    .save()
    .then(ItineraryData => {
      returnObj.success = true;
      returnObj.message = "Data added";
      returnObj.data = ItineraryData;
      res.send(returnObj);
    })
    .catch(error => {});
}

function updateItinerary(req, res) {
  let returnObj = {
    success: false,
    message: "Update Itinerary Failed",
    data: {}
  };

  let gallery = [];
  let featuredPic = null;
  if (req.files.gallery !== undefined) {
    req.files.gallery.forEach(pics => {
      gallery.push("/uploads/" + pics.filename);
    });
  }
  if (req.files.featuredImage !== undefined) {
    req.files.featuredImage.forEach(pics => {
      featuredPic = "/uploads/" + pics.filename;
    });
  }

  const itinerary = JSON.parse(req.body.itinerary);
  const tabs = JSON.parse(req.body.tabs);
  let months = JSON.parse(req.body.month);
  let category = JSON.parse(req.body.category);

  ItinerarySchema.findOne({ _id: req.body._id })
    .then(foundItineray => {
      foundItineray.pageTitle = req.body.pageTitle;
      foundItineray.metaTitle = req.body.metaTitle;
      foundItineray.shortDescription = req.body.shortDescription;
      foundItineray.noOfPeople = req.body.noOfPeople;
      foundItineray.city = req.body.city;
      foundItineray.state = req.body.state;
      foundItineray.metaDescription = req.body.metaDescription;
      foundItineray.metaKeywords = req.body.metaKeywords;
      foundItineray.description = req.body.description;
      foundItineray.itineraryDays = itinerary;
      foundItineray.tripDetail = tabs;
      foundItineray.category = category;
      foundItineray.month = months;
      foundItineray.featuredImage =
        featuredPic !== null ? featuredPic : foundItineray.featuredImage;
      foundItineray.gallery = [...foundItineray.gallery, ...gallery];
      foundItineray.price = req.body.price;
      foundItineray.budgetCategory = req.body.budgetCategory;
      foundItineray
        .save()
        .then(savedItem => {
          ItinerarySchema.findOne({ _id: req.body._id })
            .populate(
              "itineraryDays.platinumId itineraryDays.goldId itineraryDays.silverId category"
            )
            .then(foundObj => {
              if (foundObj) {
                returnObj.success = true;
                returnObj.message = "Found Itinerary";
                returnObj.data = foundObj;
                res.send(returnObj);
              } else {
                res.send(returnObj);
              }
            })
            .catch(err => {
              res.send(returnObj);
            });
        })
        .catch(err => {
          res.send(returnObj);
        });
    })
    .catch(err => {
      returnObj.message = "No Itinereay Found";
      res.send(returnObj);
    });
}

function getAllItinerary(req, res) {
  let returnObj = {
    success: false,
    message: "No Itinerary found",
    data: {}
  };
  let allItinerary = [];
  ItinerarySchema.find()
    .populate("category")
    .sort({ date: -1 })
    .then(foundItinerary => {
      if (foundItinerary) {
        allItinerary = foundItinerary;
        returnObj.success = true;
        returnObj.message = "Itinerary found";
        returnObj.data = allItinerary;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    });
}

function getAllItineraryWeb(req, res) {
  let returnObj = {
    success: false,
    message: "No Itinerary found",
    data: {}
  };

  let skip = parseInt(req.query.pageNo) * 6;
  const limit = 6;
  let allItinerary = [];
  ItinerarySchema.find()
    .sort({ date: -1 })
    .skip(skip)
    .limit(limit)
    .lean()
    .then(foundItinerary => {
      if (foundItinerary) {
        allItinerary = foundItinerary;
        returnObj.success = true;
        returnObj.message = "Itinerary found";
        returnObj.data = allItinerary;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    });
}

function deleteItinerary(req, res) {
  let returnObj = {
    success: false,
    message: "No Itinerary found"
  };
  ItinerarySchema.findOneAndRemove({ _id: req.body._id }).then(
    foundItinerary => {
      if (foundItinerary) {
        returnObj.success = true;
        returnObj.message = "Itinerary deleted";
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    }
  );
}

function searchItinerary(req, res) {
  let returnObj = {
    success: false,
    message: "No Itinerary found",
    data: {}
  };
  let andArray = [];
  let OrArray = [];
  let finalArr = [];

  if (req.body.location != "") {
    OrArray.push(
      { city: { $regex: new RegExp(req.body.location), $options: "i" } },
      { state: { $regex: new RegExp(req.body.location), $options: "i" } }
    );
    finalArr.push({ $or: OrArray });
  }
  if (req.body.category != "") {
    andArray.push({
      category: new ObjectID(req.body.category)
    });
  }
  if (req.body.month != "") {
    andArray.push({
      month: { $regex: new RegExp(req.body.month), $options: "i" }
    });
  }
  if (andArray.length != 0) {
    finalArr.push({ $and: andArray });
  }
  const obj = {
    $and: finalArr
  };

  ItinerarySchema.find(obj)
    .sort({ date: -1 })
    .then(found => {
      if (found.length != 0) {
        returnObj.success = true;
        returnObj.message = `${found.length} Itinerarie's found`;
        returnObj.data = found;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    })
    .catch(err => {
      res.send(returnObj);
    });
}

function getItineraryDetails(req, res) {
  let returnObj = {
    success: false,
    message: "No Details found",
    data: {}
  };

  ItinerarySchema.findOne({
    $or: [{ _id: req.headers._id }, { slug: req.headers.slug }]
  })
    .populate(
      "itineraryDays.platinumId itineraryDays.goldId itineraryDays.silverId reviews category"
    )
    .then(foundObj => {
      if (foundObj) {
        ItinerarySchema.find({ category: foundObj.category })
          .skip(0)
          .limit(4)
          .lean()
          .then(suggestions => {
            let itineraryObj = {
              ...foundObj._doc,
              suggestions: suggestions
            };
            returnObj.success = true;
            returnObj.message = "Found Itinerary";
            returnObj.data = itineraryObj;
            res.send(returnObj);
          })
          .catch(err => {
            let itineraryObj = {
              ...foundObj._doc,
              suggestions: []
            };
            returnObj.success = true;
            returnObj.message = "Found Itinerary";
            returnObj.data = itineraryObj;
            res.send(returnObj);
          });
      } else {
        res.send(returnObj);
      }
    })
    .catch(err => {
      returnObj.message = "Something Went wrong";
      res.send(returnObj);
    });
}

function deleteItineraryImage(req, res) {
  let returnObj = {
    success: false,
    message: "No Image found",
    data: {}
  };
  ItinerarySchema.findOne({ _id: req.body._id })
    .then(foundItinerary => {
      if (req.body.type == "featured") {
        foundItinerary.featuredImage = null;
      } else {
        const index = req.body.index;
        foundItinerary.gallery.splice(index, 1);
        foundItinerary.gallery = [...foundItinerary.gallery];
      }
      foundItinerary
        .save()
        .then(newObj => {
          ItinerarySchema.findOne({ _id: newObj._id })
            .populate(
              "itineraryDays.platinumId itineraryDays.goldId itineraryDays.silverId category"
            )
            .then(foundObj => {
              if (foundObj) {
                returnObj.success = true;
                returnObj.message = "Found Itinerary";
                returnObj.data = foundObj;
                res.send(returnObj);
              } else {
                res.send(returnObj);
              }
            })
            .catch(err => {
              returnObj.message = "Something Went wrong";
              res.send(returnObj);
            });
        })
        .catch(err => {
          returnObj.message = "Something Went Wrong";
          res.send(returnObj);
        });
    })
    .catch(err => {
      returnObj.message = "Something Went Wrong";
      res.send(returnObj);
    });
}

function getLocationsList(req, res) {
  let returnObj = {
    success: false,
    message: "No States Found",
    data: []
  };
  let array = [];
  ItinerarySchema.distinct("state")
    .then(foundState => {
      array = [...array, ...foundState];
      returnObj.success = true;
      returnObj.message = "List of states";
      returnObj.data = array;
      res.send(returnObj);
    })
    .catch(err => {
      returnObj.message = "Something Went Wrong";
      res.send(returnObj);
    });
}

export default {
  Itinerary,
  getItineraryDetails,
  getAllItinerary,
  deleteItinerary,
  searchItinerary,
  deleteItineraryImage,
  getLocationsList,
  updateItinerary,
  getAllItineraryWeb
};
