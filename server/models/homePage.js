import mongoose from "mongoose";

var Schema = mongoose.Schema;

const HomePageSchema = new mongoose.Schema({
  pageName: { type: String, default: "home", unique: true },
  popularDestinations: [],
  bestValueTrips: [{ type: Schema.Types.ObjectId, ref: "Itinerary" }],
  popularTrips: [{ type: Schema.Types.ObjectId, ref: "Itinerary" }],
  counts: {
    years: { type: Number, default: 0 },
    trips: { type: Number, default: 0 },
    travellers: { type: Number, default: 0 },
    destinations: { type: Number, default: 0 }
  }
});

export default mongoose.model("HomePageData", HomePageSchema);
