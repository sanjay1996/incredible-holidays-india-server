import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ReviewsSchema = new mongoose.Schema({
  itineraryId: { type: Schema.Types.ObjectId, ref: "Itinerary" },
  name: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  email: { type: String, default: null },
  subject: { type: String, default: null },
  rating: { type: Number, default: 0 },
  message: { type: String, default: null },
  status: { type: Boolean, default: false },
  title: { type: String, default: null },
  url: { type: String, default: null }
});

export default mongoose.model("Reviews", ReviewsSchema);
