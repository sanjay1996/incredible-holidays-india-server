import mongoose from "mongoose";

var Schema = mongoose.Schema;

const BlogSchema = new mongoose.Schema({
  blogHeader: { type: String, default: null },
  blogTitle: { type: String, default: null },
  metaTitle: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  slug: { type: String, default: null, unique: true },
  metaDescription: { type: String, default: null },
  metaKeywords: { type: String, default: null },
  featuredImage: { type: String, default: null },
  blogSections: [
    {
      image: { type: String, default: null },
      description: { type: String, default: null }
    }
  ],
  reviews: [{ type: Schema.Types.ObjectId, ref: "Reviews" }]
});

export default mongoose.model("Blog", BlogSchema);
