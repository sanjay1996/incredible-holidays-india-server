import mongoose from "mongoose";

var Schema = mongoose.Schema;

const CategoriesSchema = new mongoose.Schema({
  categoryName: { type: String, default: null, unique: true }
});

export default mongoose.model("Categories", CategoriesSchema);
