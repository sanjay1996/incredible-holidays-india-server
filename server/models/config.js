import mongoose from "mongoose";

var Schema = mongoose.Schema;

const configSchema = new mongoose.Schema({
  adminConfig: { type: String, default: "admin", unique: true },
  gaId: { type: String, default: null },
  googleMapApi: { type: String, default: null },
  title: { type: String, default: null },
  email: { type: String, default: null },
  mobile: { type: String, default: null },
  address: { type: String, default: null },
  website: { type: String, default: null },
  facebookUrl: { type: String, default: null },
  twitterUrl: { type: String, default: null },
  youtubeUrl: { type: String, default: null },
  instagramUrl: { type: String, default: null }
});

export default mongoose.model("configData", configSchema);
