import mongoose from "mongoose";

var Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
  name: { type: String, default: null },
  email: { type: String, default: null, unique: true },
  phone: { type: String, default: null },
  userType: { type: String, default: "user" },
  password: { type: String, default: null, unique: true }
});

export default mongoose.model("User", UserSchema);
