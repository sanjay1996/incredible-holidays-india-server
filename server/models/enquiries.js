import mongoose from "mongoose";

var Schema = mongoose.Schema;

const enquiresSchema = new mongoose.Schema({
  name: { type: String, default: null },
  mobile: { type: String, default: null },
  email: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  query: { type: String, default: null },
  itinerary: { type: Schema.Types.ObjectId, ref: "Itinerary", default: null },
  status: { type: String, default: "Pending" }
});

export default mongoose.model("enquires", enquiresSchema);
