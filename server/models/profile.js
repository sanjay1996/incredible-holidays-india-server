import mongoose from "mongoose";

var Schema = mongoose.Schema;

const profileSchema = new mongoose.Schema({
  name: { type: String, default: null },
  dateOfBirth: { type: String, default: null },
  qualification: { type: String, default: null },
  profilePicture: { type: String, default: null },
  about: { type: String, default: null }
});

export default mongoose.model("Profile", profileSchema);
