import mongoose from "mongoose";

var Schema = mongoose.Schema;

const MeetingSchema = new mongoose.Schema({
  venueName: { type: String, default: null },
  venueDateTime: { type: Date, default: null }
});

export default mongoose.model("Meeting", MeetingSchema);
