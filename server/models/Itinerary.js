import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ItinerarySchema = new mongoose.Schema({
  pageTitle: { type: String, default: null },
  metaTitle: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  shortDescription: { type: String, default: null },
  noOfPeople: { type: String, default: 0 },
  slug: { type: String, default: null, unique: true },
  pageUrl: { type: String, default: null },
  category: [{ type: Schema.Types.ObjectId, ref: "Categories" }],
  metaDescription: { type: String, default: null },
  metaKeywords: { type: String, default: null },
  description: { type: String, default: null },
  featuredImage: { type: String, default: null },
  gallery: [{ type: String, default: null }],
  month: [{ type: String, default: null }],
  price: [{ type: String, default: null }],
  budgetCategory: [{ type: String, default: null }],
  itineraryDays: [
    {
      platinumHotelName: { type: String, default: null },
      platinumId: { type: Schema.Types.ObjectId, ref: "hotel", default: null },
      goldHotelName: { type: String, default: null },
      goldId: { type: Schema.Types.ObjectId, ref: "hotel", default: null },
      silverHotelName: { type: String, default: null },
      silverId: { type: Schema.Types.ObjectId, ref: "hotel", default: null },
      day: { type: String, default: null },
      title: { type: String, default: null },
      dayDes: { type: String, default: null }
    }
  ],
  tripDetail: [],
  city: { type: String, default: null },
  state: { type: String, default: null },
  reviews: [{ type: Schema.Types.ObjectId, ref: "Reviews" }]
});

export default mongoose.model("Itinerary", ItinerarySchema);
