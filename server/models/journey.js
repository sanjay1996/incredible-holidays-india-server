import mongoose from "mongoose";

var Schema = mongoose.Schema;

const JourneySchema = new mongoose.Schema({
  journey: { type: String, default: null },
  image: { type: String, default: null }
});

export default mongoose.model("Journey", JourneySchema);
