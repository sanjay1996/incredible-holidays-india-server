import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ComplaintsSchema = new mongoose.Schema({
  name: { type: String, default: null },
  email: { type: String, default: null },
  mobileNo: { type: String, default: null },
  complaint: { type: String, default: null }
});

export default mongoose.model("Complaints", ComplaintsSchema);
