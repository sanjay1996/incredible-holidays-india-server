import mongoose from "mongoose";

var Schema = mongoose.Schema;

const hotelSchema = new mongoose.Schema({
  hotelName: { type: String, default: null },
  // email: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  // phone: { type: Number, default: null },
  images: [{ type: String, default: null }],
  addressLine1: { type: String, default: null },
  addressLine2: { type: String, default: null },
  city: { type: String, default: null },
  state: { type: String, default: null },
  country: { type: String, default: null },
  zipcode: { type: String, default: null }
});

export default mongoose.model("hotel", hotelSchema);
