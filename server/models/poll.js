import mongoose from "mongoose";

var Schema = mongoose.Schema;

const PollSchema = new mongoose.Schema({
  question: { type: String, default: null },
  date: { type: Date, default: Date.now() },
  options: [{}],
  response: [{ type: String, default: null }]
});

export default mongoose.model("Poll", PollSchema);
