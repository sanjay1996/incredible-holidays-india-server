import express from "express";
import authRoutes from "./auth";
import userRoutes from "./user";
import adminRoutes from "./admin";
import webRoutes from "./web";

//import csvToJson from './csvToJson';

const router = express.Router();

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("OK"));

router.get("/", (req, res) => res.send("It`s Working"));

// mount user routes at /users
router.use("/users", userRoutes);

// mount auth routes at /auth
router.use("/auth", authRoutes);

// mount admin routes t0 /admin
router.use("/admin", adminRoutes);

// mount Front-End routes to /web
router.use("/web", webRoutes);

export default router;
