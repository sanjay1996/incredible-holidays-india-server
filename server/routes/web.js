import express from "express";
import homePageCtrl from "../controllers/homePage";
import hotelCtrl from "../controllers/hotel";
import userCtrl from "../controllers/user";
import itineraryCtrl from "../controllers/itinerary";
import queryCtrl from "../controllers/enquiries";
import BlogCtrl from "../controllers/blogs";
import configCtrl from "../controllers/config";

const router = express.Router();

router.route("/homePageData").get(homePageCtrl.getHomePagedata);

router.get("/fetchHotelDetails", hotelCtrl.fetchHotelDetails);

router.get("/getItinerary", itineraryCtrl.getAllItineraryWeb);

router.get("/getItineraryDetails", itineraryCtrl.getItineraryDetails);

router.post("/search-itinerary", itineraryCtrl.searchItinerary);

router.post("/submitReview", userCtrl.reviews);

router.post("/addEnquiry", queryCtrl.addEnquiry);

router.get("/getFooterData", homePageCtrl.getFooterData);

router.get("/getCategoryList", homePageCtrl.getCategoryList);

router.get("/getBlogsList", BlogCtrl.getBlogsListWeb);

router.get("/getBlogDetails", BlogCtrl.getBlogDetails);

router.post("/submitBlogReview", userCtrl.reviewBlog);

router.get("/fetchConfig", configCtrl.fetchConfig);

router.get("/getlocationslist", itineraryCtrl.getLocationsList);

// router.get('/getFeaturedImage', profileCtrl.getFeaturedImage);

export default router;
