import express from "express";
import validate from "express-validation";
import httpStatus from "http-status";
import passport from "passport";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import paramValidation from "../../config/param-validation";
import homePageCtrl from "../controllers/homePage";
import BlogCtrl from "../controllers/blogs";
import multer from "multer";

const router = express.Router();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function(req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = "./public/uploads" + imageName;
  }
});

const uploadObj = multer({ storage: storage });

const uploadFields = uploadObj.fields([
  {
    name: "featuredImage",
    maxCount: 1
  },
  {
    name: "blogImageOne",
    maxCount: 1
  },
  {
    name: "blogImageTwo",
    maxCount: 1
  },
  {
    name: "blogImageThree",
    maxCount: 1
  }
]);

router.route("/homePageData").get(homePageCtrl.getHomePagedata);
router.route("/homePageData").post(homePageCtrl.addHomePagedata);
router
  .route("/homePageData/popular-destinations")
  .post(uploadObj.array("image", 10), homePageCtrl.popularDestinations);
router
  .route("/homePageData/popular-destinations")
  .delete(homePageCtrl.deletePopularDestination);
router.route("/addCategory").post(homePageCtrl.addCategory);
router.route("/getCategoryList").get(homePageCtrl.getCategoryList);
router.route("/deleteCategory").post(homePageCtrl.deleteCategory);
router.route("/addBlog").post(uploadFields, BlogCtrl.addBlog);
router.route("/getBlogsList").get(BlogCtrl.getBlogsList);
router.route("/getBlogDetails").get(BlogCtrl.getBlogDetails);
router.route("/deleteBlog").post(BlogCtrl.deleteBlog);
router.route("/deleteBlogImage").post(BlogCtrl.deleteBlogImage);
router.route("/editBlog").post(uploadFields, BlogCtrl.editBlog);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
// router.use((req, res, next) => {
//   passport.authenticate(
//     "jwt",
//     config.passportOptions,
//     (error, userDtls, info) => {
//       //eslint-disable-line
//       if (error) {
//         const err = new APIError(
//           "token not matched",
//           httpStatus.INTERNAL_SERVER_ERROR
//         );
//         return next(err);
//       } else if (userDtls) {
//         req.user = userDtls;
//         next();
//       } else {
//         const err = new APIError(
//           `token not matched ${info}`,
//           httpStatus.UNAUTHORIZED
//         );
//         return next(err);
//       }
//     }
//   )(req, res, next);
// });

export default router;
