import express from "express";
import validate from "express-validation";
import httpStatus from "http-status";
import passport from "passport";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import paramValidation from "../../config/param-validation";
import userCtrl from "../controllers/user";
import hotelCtrl from "../controllers/hotel";
import configCtrl from "../controllers/config";
import queryCtrl from "../controllers/enquiries";
import itineraryCtrl from "../controllers/itinerary";
import dashboardCtrl from "../controllers/dashboard";
import multer from "multer";

const router = express.Router();

// use this line for validation of params
// router.route('/register').post(validate(paramValidation.createUser), userCtrl.createUser);

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function(req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = "./public/uploads" + imageName;
  }
});

const uploadObj = multer({ storage: storage });
const uploadFields = uploadObj.fields([
  {
    name: "featuredImage",
    maxCount: 1
  },
  {
    name: "gallery",
    maxCount: 10
  }
]);

router.post("/register", userCtrl.createUser);

router.post("/reviews", userCtrl.reviews);

router.get("/getAllReviews", userCtrl.getAllReviews);

router.post("/reviewStatusUpdate", userCtrl.reviewStatusUpdate);

router.post("/reviewDelete", userCtrl.reviewDelete);

router
  .route("/addHotelDetails")
  .post(uploadObj.array("e", 10), hotelCtrl.addHotelDetails);

router
  .route("/updateHotelDetails")
  .post(uploadObj.array("e", 10), hotelCtrl.updateHotel);

router.get("/fetchHotelDetails", hotelCtrl.fetchHotelDetails);

router.post("/updateStatus", hotelCtrl.updateStatus);

router.post("/deleteHotel", hotelCtrl.deleteHotel);
router.post("/deleteHotelImage", hotelCtrl.deleteHotelImage);

router.get("/fetchConfig", configCtrl.fetchConfig);

router.post("/addEnquiry", queryCtrl.addEnquiry);

router.get("/getEnquires", queryCtrl.getEnquires);
router.post("/editEnquiryStatus", queryCtrl.editEnquiryStatus);

router.route("/Itinerary").post(uploadFields, itineraryCtrl.Itinerary);

router.get("/getItinerary", itineraryCtrl.getAllItinerary);

router.post("/deleteItinerary", itineraryCtrl.deleteItinerary);

router.post("/deleteItineraryImage", itineraryCtrl.deleteItineraryImage);
router
  .route("/updateItinerary")
  .post(uploadFields, itineraryCtrl.updateItinerary);

router.get("/dashboardReviewCount", dashboardCtrl.dashboardReviewCount);
router.get("/getlocationslist", itineraryCtrl.getLocationsList);

// router.get('/getFeaturedImage', profileCtrl.getFeaturedImage);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
  passport.authenticate(
    "jwt",
    config.passportOptions,
    (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError(
          "token not matched",
          httpStatus.INTERNAL_SERVER_ERROR
        );
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(
          `token not matched ${info}`,
          httpStatus.UNAUTHORIZED
        );
        return next(err);
      }
    }
  )(req, res, next);
});

export default router;
